// Ejer10.cpp: define el punto de entrada de la aplicación de consola.
//

#include "stdafx.h"
#include <iostream>
#include <string>

void age();
bool isValidDate(int, int, int);
float isLeapYear(int);
bool is30Month(int);
bool is31Month(int);
bool isNumber(std::string);
void yourAge(int);

int Iday;
int Imonth;
int Iyear;

int main()
{
	age();
    return 0;
}

void age() {
	std::string Sday;
	std::string Smonth;
	std::string Syear;
	do {
		do {
			std::cout << "Enter your day birth" << std::endl;
			std::cin >> Sday;
		} while (isNumber(Sday));
		do {
			std::cout << "Enter your month birth" << std::endl;
			std::cin >> Smonth;
		} while (isNumber(Smonth));
		do {
			std::cout << "Enter your month year" << std::endl;
			std::cin >> Syear;
		} while (isNumber(Syear));
		Iday = std::atoi(Sday.c_str());
		Imonth = std::atoi(Smonth.c_str());
		Iyear = std::atoi(Syear.c_str());
	} while (isValidDate(Iday,Imonth,Iyear));
	yourAge(Iyear);
}

bool isValidDate(int day, int month, int year) {
	if (day < 0 || month < 0 || year < 0) {
		std::cout << "please enter a valid date" << std::endl;
		return true;
	}
	else if (((isLeapYear(year) != 0) && month == 2 && day > 28) || ((isLeapYear(year) == 0) && month == 2 && day > 29)) {
		std::cout << "Frebruary only have 28 days or 29 on leap year, please enter a valid date" << std::endl;
		return true;
	}
	else if (is30Month(month) && day > 30) {
		std::cout << "that month only have 30 days, please enter a valid date" << std::endl;
		return true;
	}
	else if (is30Month(month) && day > 31) {
		std::cout << "that month only have 31 days, please enter a valid date" << std::endl;
		return true;
	}
	else if (month > 12) {
		std::cout << "that month is invalid, please enter a valid date" << std::endl;
		return true;
	}
	else if (year > 2017 || year < 1800) {
		std::cout << "that year no are valid, please enter a valid date" << std::endl;
		return true;
	}
	return false;
}

float isLeapYear(int year){
	return (year % 4);
}

bool is30Month(int month) {
	return (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12);
}

bool is31Month(int month) {
	return (month == 4 || month == 6 || month == 9 || month == 11);
}

bool isNumber(std::string number) {
	char aux2[100];
	strcpy_s(aux2,number.c_str());
	for (int i = 0; i < number.length();i++) {
		if (!isdigit(aux2[i])) {
			std::cout << "the date content a invalid character, please enter a valid number" << std::endl;
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			return true;
		}
	}
	return false;
}
void yourAge(int year) {
	std::cout << "your age is: "<< 2018 - year << std::endl;
}

